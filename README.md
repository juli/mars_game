# Mars Game

Derived fron the default minetst_game bundled in the Minetest engine,
ideas from the old Marssurvive mod.
As Mapgen it uses cratermg (set mapgen to singlenode in map_meta.txt)

## Installation

- Unzip the archive, rename the folder to minetest_game and
place it in .. minetest/games/

- GNU/Linux: If you use a system-wide installation place
    it in ~/.minetest/games/.

The Minetest engine can be found at [GitHub](https://github.com/minetest/minetest).

For further information or help, see:  
https://wiki.minetest.net/Installing_Mods

## Compatibility

This game was build for the Minetest 5.x.x engine.

## Licensing

See `LICENSE.txt`

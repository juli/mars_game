--NEW (for sp registration)

-- TODO: move this to config
spacesuit = {breath_timer=0,skin={}}
spacesuit.registered_spacesuits = {}
spacesuit.air_block = "gas:oxygen"

--sp manipulation
local stack_one_sp = function(inventory)
        return string.sub(inventory:get_stack("spacesuit", 1):get_name(), 1, 12)=="spacesuit:sp"
end

local set_wear_sp = function(inventory, wear)
        local name = inventory:get_stack("spacesuit", 1):get_name()
        inventory:set_stack("spacesuit", 1,ItemStack({name=name,wear=wear}))
end

local player_get_sp = function(inventory)
        return spacesuit.registered_spacesuits[inventory:get_stack("spacesuit", 1):get_name()]
end
	
--skin
local player_attach_sp = function(player)
        local sp=player_get_sp(player:get_inventory())
        spacesuit.skin[player:get_player_name()]=player:get_properties().textures
	player:set_properties({visual = "mesh",textures = sp.textures, visual_size = {x=1, y=1}})
	--armor:
	player:set_armor_groups({fleshy = 100-sp.protection})
end

local player_deattach_sp = function(player)
	local textures = spacesuit.skin[player:get_player_name()]
        spacesuit.skin[player:get_player_name()]=nil
	player:set_properties({visual = "mesh",textures = textures, visual_size = {x=1, y=1}})
	--armor:
	player:set_armor_groups({fleshy = 100})
end



spacesuit.register_spacesuit = function(name, inventory_image, protection, textures)
        local tool_name = "spacesuit:sp" .. name
	spacesuit.registered_spacesuits[tool_name] = {sp_name=name, protection=protection, textures=textures}
	minetest.register_tool(tool_name, {
	        description = "Spacesuit " .. name .. " (wear slot 1)",
	        range = 4,
	        inventory_image = inventory_image,
        })
end

spacesuit.register_spacesuit("", "spacesuit_sp_white_inv.png", 0, {"spacesuit_sp_white.png"})
spacesuit.register_spacesuit("black", "spacesuit_sp_black_inv.png", 33, {"spacesuit_sp_black.png"})

--GASSBOTTLE
minetest.register_node("spacesuit:air_gasbottle", {
	description = "oxygen-gasbottle",
	tiles = {"gasbottle_full.png"},
	inventory_image = "gasbottle_full.png",
	drawtype = "plantlike",
	groups = {dig_immediate=3},
	sounds = default.node_sound_stone_defaults(),
	is_ground_content = false,
	paramtype = "light",
	on_use = function(itemstack, user, pointed_thing)
		local pos = pointed_thing.above
		if pos == nil then
			print(dump(pointed_thing))
		end
		if gas.add_concentration(pos, "gas:oxygen", 64) ~= -1 then
			itemstack:set_count(itemstack:get_count()-1)
			user:get_inventory():add_item("spacesuit", "spacesuit:air_gasbottle_empty")
			return itemstack
		end
	end,
})

minetest.register_node("spacesuit:air_gasbottle_empty", {
        description = "empty oxygen-gasbottle",
        tiles = {"gasbottle_empty.png"},
	inventory_image = "gasbottle_empty.png",
	drawtype = "plantlike",
	groups = {dig_immediate=3},
	sounds = default.node_sound_stone_defaults(),
	is_ground_content = false,
	paramtype = "light",
    	on_use = function(itemstack, user, pointed_thing)
		local upos = user:get_pos()
		local pos = {x=upos.x, y=math.ceil(upos.y)+1, z=upos.z}
		if gas.get_name(pos) ~= "gas:oxygen" then
			minetest.chat_send_player(user:get_player_name(), "you are not inside oxygen!")
			print(dump(upos))
			print(dump(pos))
			return
		end
		local meta = minetest.get_meta(pos)
		local current_value = meta:get_int("gas:concentration")
		if current_value > 65 then
			meta:set_int("gas:concentration", 1)
			itemstack:set_count(itemstack:get_count()-1)
			user:get_inventory():add_item("spacesuit", "spacesuit:air_gasbottle")
			minetest.chat_send_player(user:get_player_name(), "oxygen-bottle refilled!") 
			return itemstack
		else
			minetest.chat_send_player(user:get_player_name(), "oxygen pressure to low (it is "..current_value.." and must be 65)!") 
		end
	end,
})

--CRAFT
minetest.register_craft({
	output = "spacesuit:sp",
	recipe = {
		{"spacesuit:sp","spacesuit:air_gasbottle",""}

	},
})


minetest.register_craft({
	output = "spacesuit:spblack",
	recipe = {
		{"","wool:black","",},
		{"dye:black", "spacesuit:sp", "dye:black",},
		{"dye:black", "spacesuit:air_gasbottle", "dye:black",},
	},
})

minetest.register_craft({
	output = "spacesuit:sp",
	recipe = {
		{"","wool:white","",},
		{"homedecor:plastic_sheeting", "spacesuit:air_gasbottle", "homedecor:plastic_sheeting",},
		{"homedecor:plastic_sheeting", "mesecons:wire_00000000_off", "homedecor:plastic_sheeting",},
	},
})

minetest.register_craft({
	output = "spacesuit:air_gasbottle 2",
	recipe = {
		{"","default:copper_ingot",""},
		{"default:steel_ingot","","default:steel_ingot"},
		{"default:steel_ingot","","default:steel_ingot"},
	}
})

--FUNCTIONS
local damage = function(node, player)
	if node == "air" then								--(no spacesuit and in default air: lose 8 hp)
		player:set_hp(player:get_hp()-8)
	elseif node ~= spacesuit.air_block then						--(no spacesuit and inside something else: lose 1 hp)
		player:set_hp(player:get_hp()-1)
	end
end

minetest.register_globalstep(function(dtime)
	spacesuit.breath_timer=spacesuit.breath_timer+dtime
	if spacesuit.breath_timer<2 then return end
	spacesuit.breath_timer=0
	for i, player in pairs(minetest.get_connected_players()) do
		local n=player:getpos()
		n=minetest.get_node({x=n.x,y=n.y+1,z=n.z}).name
		local inv=player:get_inventory()
		
		if stack_one_sp(inv) then
			local wear = inv:get_stack("spacesuit", 1):get_wear()
			if n == spacesuit.air_block then
				local new_wear = wear-(65534/20)
				if new_wear < 0 then new_wear = 0 end
				set_wear_sp(inv, new_wear)
			else
				if wear < 65533 then
					local new_wear=wear+ (65534/900)
					if new_wear >= 65533 then new_wear = 65533 end
					set_wear_sp(inv, new_wear)
					player:set_breath(11)
				else
					local bottle = {name="spacesuit:air_gasbottle", count=1, wear=0, metadata=""}
					print('bottle', inv:contains_item("spacesuit", bottle))
					if inv:contains_item("spacesuit", bottle) then
						local removed = inv:remove_item("spacesuit", bottle)
						set_wear_sp(inv, 0)
						removed:set_name(removed:get_name() .. "_empty")
						inv:add_item("spacesuit", removed)
						minetest.sound_play("marssurvive_pff", {pos=pos, gain = 1, max_hear_distance = 8,})
						
						if inv:contains_item("spacesuit", bottle) then --have one or more bottles 
							minetest.chat_send_player(player:get_player_name(), "Warning: one more air-gasbottle is empty!")		
						else
							minetest.chat_send_player(player:get_player_name(), "Warning: have none air-gssbottle left!")
						end
					else
						--no bottle, no wear 
						damage(n, player)
					end
				end
			end
			--skin:
			if (spacesuit.skin[player:get_player_name()] == nil) then
				player_attach_sp(player)
			end
		else
			if (spacesuit.skin[player:get_player_name()] ~= nil) then
				player_deattach_sp(player)
			end
			damage(n, player)
		end
	end
end)

minetest.register_on_joinplayer(function(player)
	if stack_one_sp(player:get_inventory()) then
		player_attach_sp(player)
	end
end)

-- clear player data
minetest.register_on_leaveplayer(function(player)
	spacesuit.skin[player:get_player_name()]=nil
end)

dofile(minetest.get_modpath(minetest.get_current_modname()).."/inventory.lua")

minetest.log("[MOD] spacesuit loaded")

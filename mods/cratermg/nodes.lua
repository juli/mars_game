--[[
	Crater MG - Crater Map Generator for Minetest
	(c) Pierre-Yves Rollo

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published
	by the Free Software Foundation, either version 2.1 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

local planet_name = "Planet"

-- Basic nodes registration
---------------------------

minetest.register_node("cratermg:stone", {
	description = planet_name.." Stone",
	tiles = {"default_stone.png^[colorize:#F408"},
	groups = {cracky = 2, stone = 1},
	drop = "cratermg:sediment",
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("cratermg:sediment", {
	description = planet_name.." Sediments",
	tiles = {"default_cobble.png^[colorize:#F408"},
	groups = {cracky = 2, stone = 2},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("cratermg:dust", {
	description = planet_name.." Dust",
	tiles = {"default_sand.png^[colorize:#F408"},
	groups = {crumbly = 3, falling_node = 1, sand = 1},
	sounds = default.node_sound_sand_defaults(),
})

-- decoration

local nodes_g_s={
{"#cf7d67","tiny",0.25},
{"#bf7d67","small",0.27},
{"#cf7d57","medium",0.3},
}

cratermg.stones = {}
for i = 1, #nodes_g_s, 1 do
    minetest.register_node("cratermg:stone_" .. nodes_g_s[i][2], {
	description = "Stone " .. nodes_g_s[i][2],
	drawtype = "mesh",
	mesh = "stone1.obj",
	visual_scale = nodes_g_s[i][3],
	tiles = {"default_stone.png^[colorize:"..nodes_g_s[i][1]},
	groups = {dig_immediate=3,not_in_creative_inventory=0,stone=1},
	paramtype = "light",
	paramtype2 = "facedir",
	sounds = default.node_sound_stone_defaults(),
	sunlight_propagates = true,
	is_ground_content = false,
        drop = {
            	max_items = 1,
            	items = {
                	
			{
                	    rarity = 200,
                	    items = {"default:diamond"},
                	},
			{
                	    rarity = 160,
                	    items = {"default:gold_lump"},
                	},
			{
                	    rarity = 80,
                	    items = {"frozen:apple"},
                	},
			{
                	    rarity = 50,
                	    items = {"frozen:seed"},
                	},
		}
	},

	selection_box = {
		type = "fixed",
		fixed = {-0.3, -0.5, -0.3, 0.3, 0.-0.25, 0.3}
	},
        collision_box = {
		type = "fixed",
		fixed = {{-0.3, -0.5, -0.3, 0.3, 0.-0.25, 0.3},}},
    })
    cratermg.stones[i] = minetest.get_content_id("cratermg:stone_" .. nodes_g_s[i][2])
end

-- Ore nodes registration
-------------------------

minetest.register_node("cratermg:stone_with_coal", {
	description = "Coal Ore",
	tiles = {"default_stone.png^[colorize:#F408^default_mineral_coal.png"},
	groups = {cracky = 3},
	drop = "default:coal_lump",
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("cratermg:stone_with_iron", {
	description = "Iron Ore",
	tiles = {"default_stone.png^[colorize:#F408^default_mineral_iron.png"},
	groups = {cracky = 2},
	drop = "default:iron_lump",
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("cratermg:stone_with_copper", {
	description = "Copper Ore",
	tiles = {"default_stone.png^[colorize:#F408^default_mineral_copper.png"},
	groups = {cracky = 2},
	drop = "default:copper_lump",
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("cratermg:stone_with_tin", {
	description = "Tin Ore",
	tiles = {"default_stone.png^[colorize:#F408^default_mineral_tin.png"},
	groups = {cracky = 2},
	drop = "default:tin_lump",
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("cratermg:stone_with_mese", {
	description = "Mese Ore",
	tiles = {"default_stone.png^[colorize:#F408^default_mineral_mese.png"},
	groups = {cracky = 1},
	drop = "default:mese_crystal",
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("cratermg:stone_with_gold", {
	description = "Gold Ore",
	tiles = {"default_stone.png^[colorize:#F408^default_mineral_gold.png"},
	groups = {cracky = 2},
	drop = "default:gold_lump",
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("cratermg:stone_with_diamond", {
	description = "Diamond Ore",
	tiles = {"default_stone.png^[colorize:#F408^default_mineral_diamond.png"},
	groups = {cracky = 1},
	drop = "default:diamond",
	sounds = default.node_sound_stone_defaults(),
})

-- Mineral nodes registration
-----------------------------

minetest.register_node("cratermg:sediment_with_iron", {
	description = "Iron Ore",
	tiles = {"default_cobble.png^[colorize:#F408^default_mineral_iron.png"},
	groups = {cracky = 2},
	drop = "default:iron_lump",
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("cratermg:sediment_with_copper", {
	description = "Copper Ore",
	tiles = {"default_cobble.png^[colorize:#F408^default_mineral_copper.png"},
	groups = {cracky = 2},
	drop = "default:copper_lump",
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("cratermg:sediment_with_tin", {
	description = "Tin Ore",
	tiles = {"default_cobble.png^[colorize:#F408^default_mineral_tin.png"},
	groups = {cracky = 2},
	drop = "default:tin_lump",
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("cratermg:sediment_with_mese", {
	description = "Mese Ore",
	tiles = {"default_cobble.png^[colorize:#F408^default_mineral_mese.png"},
	groups = {cracky = 1},
	drop = "default:mese_crystal",
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("cratermg:sediment_with_gold", {
	description = "Gold Ore",
	tiles = {"default_cobble.png^[colorize:#F408^default_mineral_gold.png"},
	groups = {cracky = 2},
	drop = "default:gold_lump",
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("cratermg:sediment_with_diamond", {
	description = "Diamond Ore",
	tiles = {"default_cobble.png^[colorize:#F408^default_mineral_diamond.png"},
	groups = {cracky = 1},
	drop = "default:diamond",
	sounds = default.node_sound_stone_defaults(),
})


-- Override config
------------------

cratermg.materials.vacuum      = minetest.get_content_id("air")
cratermg.materials.hills       = minetest.get_content_id("cratermg:stone")
cratermg.materials.mare        = minetest.get_content_id("cratermg:stone")
cratermg.materials.crater_edge = minetest.get_content_id("cratermg:sediment")
cratermg.materials.crater_fill = minetest.get_content_id("cratermg:sediment")
cratermg.materials.dust        = minetest.get_content_id("cratermg:dust")

-- Ores registration
--------------------

cratermg.register_ore({
	noise = {scale = 1, octaves = 3, persist = 1, offset = -1.3},
	ore = "cratermg:stone_with_mese", spawns_in = "cratermg:stone"
})

cratermg.register_ore({
	noise = {scale = 1, octaves = 3, persist = 1, offset = -1.3},
	ore = "cratermg:stone_with_diamond", spawns_in = "cratermg:stone"
})

cratermg.register_ore({
	noise = {scale = 1, octaves = 1, persist = 1, offset = -0.3},
	ore = "cratermg:stone_with_iron", spawns_in = "cratermg:stone"
})

cratermg.register_ore({
	noise = {scale = 1, octaves = 1, persist = 1, offset = -0.4},
	ore = "cratermg:stone_with_copper", spawns_in = "cratermg:stone"
})

cratermg.register_ore({
	noise = {scale = 1, octaves = 2, persist = 1, offset = -1},
	ore = "cratermg:stone_with_tin", spawns_in = "cratermg:stone"
})

cratermg.register_ore({
	noise = {scale = 2, octaves = 2, persist = 1, offset = -1.5},
	ore = "cratermg:stone_with_coal", spawns_in = "cratermg:stone"
})

cratermg.register_ore({
	noise = {scale = 1, octaves = 1, persist = 1, offset = -0.5},
	ore = "default:ice", spawns_in = "cratermg:stone"
})

-- Debris registration
----------------------

cratermg.register_debris({
	name = minetest.get_name_from_content_id(cratermg.materials.crater_fill),
	chance = 20,
})

cratermg.register_debris({
	name = "cratermg:sediment_with_copper",
	chance = 1,
})

cratermg.register_debris({
	name = "cratermg:sediment_with_iron",
	chance = 5,
})

cratermg.register_debris({
	name = "cratermg:sediment_with_tin",
	chance = 1,
})

cratermg.register_debris({
	name = "cratermg:sediment_with_mese",
	chance = 1,
})

cratermg.register_debris({
	name = "cratermg:sediment_with_diamond",
	chance = 1,
})

cratermg.register_debris({
	name = "cratermg:sediment_with_gold",
	chance = 2,
})

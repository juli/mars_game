toilet = {}
toilet.sit = function(pos, node, clicker, pointed_thing)
	local player_name = clicker:get_player_name()
	local objs = minetest.get_objects_inside_radius(pos, 0.1)
	local vel = clicker:get_player_velocity()
	local ctrl = clicker:get_player_control()

	if default.player_attached[player_name] then
		pos.y = pos.y - 0.5
		clicker:setpos(pos)
		clicker:set_eye_offset({x=0, y=0, z=0}, {x=0, y=0, z=0})
		clicker:set_physics_override(1, 1, 1)
		default.player_attached[player_name] = false
		default.player_set_animation(clicker, "stand", 30)
		print("stand")

	elseif not default.player_attached[player_name] and node.param2 <= 3 and
			not ctrl.sneak and vector.equals(vel, {x=0,y=0,z=0}) then

		clicker:set_eye_offset({x=0, y=-7, z=2}, {x=0, y=0, z=0})
		clicker:set_physics_override(0, 0, 0)
		local player_pos = {x=pos.x, y=pos.y+0.5, z=pos.z}
		clicker:setpos(player_pos)
		default.player_attached[player_name] = true
		default.player_set_animation(clicker, "sit", 30)

		if     node.param2 == 0 then clicker:set_look_yaw(3.15)
		elseif node.param2 == 1 then clicker:set_look_yaw(7.9)
		elseif node.param2 == 2 then clicker:set_look_yaw(6.28)
		elseif node.param2 == 3 then clicker:set_look_yaw(4.75) end

		local t_meta = minetest.get_meta(pos)
		local t_value = t_meta:get_int("level") or 0;

		local meta = clicker:get_meta()
		local value = meta:get_int("toilet") or 0;

		t_value = t_value + value;
		if t_value > 10 then
			value = t_value - 10
			t_value = 10
			minetest.chat_send_player(player_name, "toilet is full!")
		else
			value = 0
		end

		t_meta:set_int("level", t_value)
		t_meta:set_string("infotext", "fill level: ".. t_value .."/10")
		meta:set_int("toilet", value)
		hb.change_hudbar(clicker, "toilet", value)
	end
end

minetest.register_node('toilet:toilet_open', {
	description = 'Toilet',
	drawtype = 'mesh',
	mesh = 'FM_toilet_open.obj',
	tiles = {{name='default_coral_skeleton.png'},{name='default_wood.png'}},
	groups = {choppy=2, oddly_breakably_by_hand=2, furniture=1, not_in_creative_inventory=1},
	--inventory_image = 'fm_chair_stone.png',
	paramtype = 'light',
	paramtype2 = 'facedir',
	drop = 'ma_pops_furniture:toilet_close',
	selection_box = {
		type = 'fixed',
		fixed = {
			{-.35, -.5, -.35, .35, 0, .5}, -- Right, Bottom, Back, Left, Top, Front
			{-.35, 0, .2, .35, .5, .5},
			}
		},
	collision_box = {
		fixed = {
			{-.35, -.5, -.35, .35, 0, .5}, -- Right, Bottom, Back, Left, Top, Front
			{-.35, 0, .2, .35, .5, .5},
			}
		},
	on_rightclick = function(pos, node, clicker, pointed)
		toilet.sit(pos, node, clicker, pointed)
		end,
	on_punch = function (pos, node, puncher)
		local groups = puncher:get_wielded_item():get_definition().groups
		if groups.shovel and (groups.shovel > 0) then
			local meta = minetest.get_meta(pos)
			local value = meta:get_int("level") or 1
			puncher:get_inventory():add_item("main", "toilet:compost_1 ".. value)
			meta:set_int("level", 0)
			meta:set_string("infotext", "fill level: 0/10 (clear)")
			return
		end
		if groups.pickaxe and (groups.pickaxe > 0) then
			minetest.dig_node(pos)
			puncher:get_inventory():add_item("main", "toilet:toilet_close")
			return
		end
		node.name = "toilet:toilet_close"
		minetest.swap_node(pos, node)
	end,
})

minetest.register_node('toilet:toilet_close', {
	description = 'Toilet',
	drawtype = 'mesh',
	mesh = 'FM_toilet_close.obj',
	tiles = {{name='default_coral_skeleton.png'},{name='default_wood.png'}},
	groups = {choppy=2, oddly_breakably_by_hand=2, furniture=1},
	paramtype = 'light',
	paramtype2 = 'facedir',
	selection_box = {
		type = 'fixed',
		fixed = {
			{-.35, -.5, -.35, .35, 0, .5}, -- Right, Bottom, Back, Left, Top, Front
			{-.35, 0, .2, .35, .5, .5},
			}
		},
	collision_box = {
		fixed = {
			{-.35, -.5, -.35, .35, 0, .5}, -- Right, Bottom, Back, Left, Top, Front
			{-.35, 0, .2, .35, .5, .5},
			}
		},
	on_rightclick = function(pos, node, clicker)
		toilet.sit(pos, node, clicker)
		end,
	on_punch = function (pos, node, puncher)
		node.name = "toilet:toilet_open"
		minetest.swap_node(pos, node)
	end,
})

hb.register_hudbar("toilet", "0xFFFF00", "Nature's call", {bar="hudbars_bar_toilet.png"}, 0, 10, false)

minetest.register_on_joinplayer(function(player) 
	local meta = player:get_meta()
	local value = 5
	if meta:get_int("toilet") ~= nil then
		value = meta:get_int("toilet")
	end
	meta:set_int("toilet", value)
	hb.init_hudbar(player, "toilet", value)
end)

minetest.register_on_item_eat(function(hp_change, replace_with_item, itemstack, player, pointed_thing)
	local meta = player:get_meta()
	local value = 5
	if meta:get_int("toilet") ~= nil then
		value = meta:get_int("toilet")
	end
	value = value + 1;
	if value > 10 then
		value = 10
		player:set_hp(player:get_hp()-2)
	end
	meta:set_int("toilet", value)
	hb.change_hudbar(player, "toilet", value)
end)


steps = {
	"default_dirt.png^[colorize:#222222CC",
	"default_dirt.png^[colorize:#22222288",
	"default_dirt.png^[colorize:#22222244",
	"default_dirt.png^[colorize:#22222211",
}

local nodes = {}
for i, v in ipairs(steps) do
	local node_name = "toilet:compost_"..i
	minetest.register_node(node_name, {
		description = "Compost (level " .. i ..")",
		tiles = {v},
		groups = {crumbly=3},
	})
	nodes[i] = node_name
end

toilet.use_gas = false
if minetest.get_modpath("gas") then
	toilet.use_gas = true
	gas.register_gas("toilet:biogas", {tiles = {"gas.png^[colorize:#33BB33CC"}, post_effect_color = {a = 180, r = 20, g = 120, b = 20}, damage_per_second = 5}, 2)

end


minetest.register_abm{
        label = "compostation",
	nodenames = nodes,
	interval = 10,
	chance = 3,
	action = function(pos, node)
		local level = 4
		for i, v in ipairs(nodes) do
			if v == node.name then
				level = i
			end
		end
		level = level+1
		local name = "toilet:compost_" .. level
		if level > 4 then
			name = "default:dirt"
		end
		minetest.set_node(pos, {name = name})
		if toilet.use_gas then
			gas.add_concentration({x=pos.x, y=pos.y+1, z=pos.z}, "toilet:biogas", 64)
		end
	end,
}

minetest.register_craft({
	output = "toilet:toilet_close",
	recipe = {
		{"group:stone", "", ""},
		{"group:stone", "", "group:stone"},
		{"group:stone", "group:stone", "group:stone"},
	}
})


minetest.log("[MOD] toilet loaded")

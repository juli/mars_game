-- gave_initial_stuff/init.lua

local stuff_string = 
		"spacesuit:sp,spacesuit:air_gasbottle 2,default:pick_steel,default:shovel_steel," ..
		"farming:bread 30,default:obsidian_glass,craftguide:book,misc:led 4"

give_initial_stuff = {
	items = {}
}

function give_initial_stuff.give(player)
	minetest.log("action",
			"Giving initial stuff to player " .. player:get_player_name())
	local inv = player:get_inventory()
	for _, stack in ipairs(give_initial_stuff.items) do
		inv:add_item("main", stack)
	end
end

function give_initial_stuff.add(stack)
	give_initial_stuff.items[#give_initial_stuff.items + 1] = ItemStack(stack)
end

function give_initial_stuff.clear()
	give_initial_stuff.items = {}
end

function give_initial_stuff.add_from_csv(str)
	local items = str:split(",")
	for _, itemname in ipairs(items) do
		give_initial_stuff.add(itemname)
	end
end

function give_initial_stuff.set_list(list)
	give_initial_stuff.items = list
end

function give_initial_stuff.get_list()
	return give_initial_stuff.items
end

give_initial_stuff.add_from_csv(stuff_string)
minetest.register_on_newplayer(give_initial_stuff.give)


minetest.register_chatcommand("give_initial_stuff", {                                                                                                 
        params = "<playername>",                                         
        description = "Give initial stuff to player",
        privs = {server=true},                
        func = function(name, param)                              
		give_initial_stuff.give(minetest.get_player_by_name(param)) 
        end                     
})   
minetest.log("[MOD] give_initial_stuff loaded")

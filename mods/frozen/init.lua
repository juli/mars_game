
frozen = {}
frozen.register = function(src_block, dst_block) 
	frozen[src_block] = dst_block
end


minetest.register_abm{
        label = "unfreeze",
	nodenames = {"group:frozen"},
	interval = 1,
	chance = 1,
	action = function(pos)
		if minetest.find_node_near(pos, 1, {"air"}) == nil and minetest.get_node_light(pos) >= 13 then
			local name = minetest.get_node(pos).name
			local result = frozen[name]
			if type(result) == "function" then
				result(pos)
			else
				minetest.set_node(pos, {name = frozen[name]})
			end
		end
	end,
}

-- actual nodes

minetest.register_node("frozen:apple", {
	description = "Frozen Apple",
	drawtype = "plantlike",
	tiles = {"default_apple.png^[colorize:#bbbbffaa"},
	inventory_image = "default_apple.png^[colorize:#bbbbffaa",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	is_ground_content = false,
	selection_box = {
		type = "fixed",
		fixed = {-3 / 16, -7 / 16, -3 / 16, 3 / 16, 4 / 16, 3 / 16}
	},
	groups = {fleshy = 3, dig_immediate = 3, frozen = 1},
	sounds = default.node_sound_glass_defaults(),
})

frozen.register("frozen:apple", function(pos) minetest.set_node(pos, {name = "default:apple", param2 = 1}) end)

minetest.register_node("frozen:seed", {
	description = "Frozen Seed", 
	tiles = {"farming_cotton_seed.png^[colorize:#bbbbff99"},
	inventory_image = "farming_cotton_seed.png^[colorize:#bbbbff99",
	drawtype = "signlike",
	paramtype = "light",
	paramtype2 = "wallmounted",
	walkable = false,
	sunlight_propagates = true,
	is_ground_content = false,
	selection_box = {
		type = "fixed",
		fixed = {-0.5, -0.5, -0.5, 0.5, -5/16, 0.5},
	},
	groups = {dig_immediate = 3, frozen = 1},
})

frozen.register("frozen:seed", function(pos) 
	if math.random(2) == 1 then
		minetest.set_node(pos, {name = "farming:seed_wheat", param2 = 1}) 
	else
		minetest.set_node(pos, {name = "farming:seed_cotton", param2 = 1}) 
	end
end)
minetest.log("[MOD] frozen loaded")


minetest.register_abm(
{
    label = "generate_oxygen",
    nodenames = {"scifi_nodes:plant8", "scifi_nodes:flower3", "scifi:nodes:flower4", "scifi_nodes:plant2", "scifi_nodes:flower2", "scifi_nodes:flower1", "group:leaves"},
    interval = 20,
    chance = 3,
    action = function(pos, node)
	if minetest.get_item_group(node.name, "leaves") > 0 then
		if not minetest.find_node_near(pos, 2, "group:tree") then
			return
		end
	end
       	gas.add_concentration({x=pos.x, y=pos.y+1,z=pos.z}, "gas:oxygen", 8)
    end,
})
minetest.log("[MOD] air_plants loaded")

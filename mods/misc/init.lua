minetest.register_craft({
	output = "default:sapling",
	recipe = {
		{"default:apple"},
		{"default:dirt"},
	}
})

minetest.register_node(":default:cloud", {
	description = "Cloud",
	tiles = {"default_cloud.png"},
	is_ground_content = false,
	sounds = default.node_sound_defaults(),
	groups = {not_in_creative_inventory = 1},
	--drawtype = "airlike",
})

minetest.unregister_item("default:torch")

local modpath = minetest.get_modpath(minetest.get_current_modname())
dofile(modpath.."/furnace.lua")

-- light

minetest.register_node("misc:clight", {
	description = "Ceiling light",
	tiles = {"default_cloud.png"},
	drawtype = "nodebox",
	groups = {snappy = 3, not_in_creative_inventory=0},
	sounds = default.node_sound_glass_defaults(),
	is_ground_content = false,
	paramtype = "light",
	paramtype2 = "facedir",
	node_box = {type="fixed",fixed={-0.2,0.4,-0.4,0.2,0.6,0.4}},
	light_source=14,
})

minetest.register_node("misc:led", {
	description = "Light emitting diode (LED)",
	tiles = {"misc_led.png"},
	inventory_image = "misc_led.png",
	wield_image = "misc_led.png",
	drawtype = "signlike",
	groups = {snappy = 3, attached_node = 2},
	paramtype = "light",
	paramtype2 = "wallmounted",
	walkable = false,
	sunlight_propagates = true,
	selection_box = {
			  type = "wallmounted",
			  wall_bottom = {-0.5, -0.5, -0.5, 0.5, -5/32, 0.5}
			},

	light_source = 13,
})


minetest.register_craft({
	type = "shapeless",
	output = "misc:led 4",
	recipe = {"mesecons_materials:silicon", "default:glass"},
})

minetest.register_craft({
	output = "misc:clight 3",
	recipe = {{"default:glass","default:glass","default:glass"},
		{"","default:mese_crystal_fragment",""}}
})

-- small inventory
minetest.register_on_joinplayer(function(player)
	player:get_inventory():set_size("main", 8)
end)
minetest.log("[MOD] misc loaded")
